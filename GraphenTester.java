import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import control.Controller;

/**
 * Dies ist die Startklasse für die GUI des Graphentesters
 * In BlueJ gibt es teilweise Probleme, dass die Menüs bei Mausklick nicht
 * offen bleiben. Dann bitte das Programm über die Main-Funktion starten.
 * 
 * @author Dirk Zechnall, Thomas Schaller
 * @version 28.02.2023 (v7.0)
 * 
 */

public class GraphenTester extends Application {

    @Override
    public void start(Stage primaryStage) {
       try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/graphenalgorithmen.fxml"));
        Controller c = new Controller();
        c.setStage(primaryStage);                             
        loader.setController(c);
        VBox root = (VBox) loader.load();
        Scene scene = new Scene(root);
        Image icon = new Image("/view/icon.png");
        primaryStage.getIcons().add(icon);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest(e ->
                {
                    c.saveAktConfig();
                    
                    c.mBeenden(null);
                    System.exit(0);
                });
        
    
    } 
       catch(Exception e)    {
        System.out.println(e);
        e.printStackTrace();
    }
    }
    public static void main(String[] args) {
      new Thread(() -> Application.launch(GraphenTester.class)).start();
    }
}
