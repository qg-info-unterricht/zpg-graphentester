package control;

import imp.*;
import graph.*;
import algorithmen.*;

import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.text.*;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.stage.*;          // Dateiöffnen / Speichern-Dialog
import java.io.File;  

import java.util.List;
import java.util.regex.Pattern;

import javafx.collections.ObservableList;

/**
 * Die Klasse HauptTabMitController stellt einen Tab inclusive ihres Controllers
 * für das Hauptfenster des Graphentesters dar. Einzelne Knoten oder Kanten können 
 * selektiert werden.
 *
 * @author Thomas Schaller
 * @version v6.7 (9.12.2020)
 */

public class HauptTabMitController extends TabMitController {

 

    public HauptTabMitController(Graph graph, GraphOptions options) {
        this.graph = graph;
        this.options = options;
        setClosable(false);
    }

    public void initialize() {
  
        super.initialize();

        
    }





 
}
