package control;

import imp.*;
import graph.*;

import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.scene.layout.*;
import javafx.scene.Node;
import javafx.scene.text.*;
import javafx.geometry.Pos;
import javafx.stage.*;          // Dateiöffnen / Speichern-Dialog
import java.io.File;  

import java.util.List;
import javafx.collections.ObservableList;
/**
 * Die Klasse TabMitController stellt die Oberklasse für alle Tabs des Graphentesters
 * dar.
 *
 * @author Thomas Schaller
 * @version v6.7 (9.12.2020)
 */

public class TabMitController extends Tab {

    protected Graph graph;
    protected GraphOptions options;

    @FXML
    protected GraphPlotter viewer;


    public void initialize() {


     /*   viewer.setHvalue(0.5);
        viewer.setVvalue(0.5);*/

    }

    protected void tabOeffnen(GraphOptions taboptions) {
        try {  // try-catch ist notwendig, um Fehler durch fehlende Dateien abzufangen.

            UnterTabMitController newtab = new UnterTabMitController(graph, taboptions);
            newtab.setText("Teilgraph");
            getTabPane().getTabs().add(newtab);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/graphtab.fxml"));
            loader.setController(newtab);
            newtab.setContent((Node) loader.load());
            getTabPane().getSelectionModel().select(newtab);

        } 
        catch(Exception e)    {
            System.out.println(e);
        }     
    }

    public GraphOptions getGraphOptions() {
        return options;
    }

    public void update() {
        viewer.updateImage();
    }

    public void setGraph(Graph graph, GraphOptions options) {
        this.graph = graph;
        this.options = options;
        if (viewer != null) viewer.setGraph(graph,options);
        update();
    }
    
    public GraphPlotter getViewer() {
        return viewer;
    }

}
