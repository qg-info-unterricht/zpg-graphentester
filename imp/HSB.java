package imp;

import java.awt.Color;

/**
 * Hilfsklasse für das HSB-Farbmodell
 *
 * @author Thomas Schaller
 * @version V1.0 14.01.2020
 */

public class HSB 
{
    /**
     * Liefert den Farbton als Winkel zwischen 0.0° und 360.0°.
     * @param Color c Farbe, deren Farbton bestimmt werden soll.
     * @return Farbton 
     */
    public static double getHue(Color c) {
        float[] hsb = Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), null);
        return hsb[0];
    }

    /**
     * Liefert die Sättigung als Wert zwischen 0.0 (0%) und 1.0 (100%).
     * @param Color c Farbe, deren Sättigung bestimmt werden soll.
     * @return Sättigung
     */
    public static double getSaturation(Color c) {
        float[] hsb = Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), null);
        return hsb[1];
    }

    /**
     * Liefert die Helligkeit als Wert zwischen 0.0 (0%) und 1.0 (100%).
     * @param Color c Farbe, deren Helligkeit bestimmt werden soll.
     * @return Sättigung
     */    
    public static double getBrightness(Color c) {
        float[] hsb = Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), null);
        return hsb[2];
    }

    /**
     * Liefert ein Java-Color-Objekt mit den angegebenen HSB-Werten.
     * @param h Farbton (Hue)
     * @param s Sättigung (Saturation)
     * @param b Brightness (Helligkeit)
     * @return Java-Color-Objekt
     */
    public static Color getColor(double h, double s, double b) {
        return new Color(Color.HSBtoRGB((float) h, (float) s, (float) b));
    }

}
