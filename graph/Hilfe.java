package graph;


public interface Hilfe {
    
    public abstract void loescheAlles();
    
    public abstract void append(String text);
        
    public void indentMore() ;
    
    public void indentLess() ;
    
    public void setGraphPlotter(GraphPlotter gp);
    
    public void setReviewAllowed(boolean a);
}  