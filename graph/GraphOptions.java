package graph;
import imp.*;
import java.util.Arrays;

/**
 * Die Klasse GraphOptions speichert, wie ein Graph in einem Fenster vom 
 * GraphPlotter angezeigt wird.
 *
 * @author Thomas Schaller
 * @version v7.0 (28.02.2023)
 * v7.0 Angezeigte Informationen bei Knoten/Kanten können ausgewählt werden
 * 
 */
public class GraphOptions
{
    // Graph 
    Graph g;
    
    // Bild
    public String  bildDatei = "";
    public boolean bildAnzeigen = false;

    // Anzeigeeinstellungen 
    public int vertexSize = 30;
    public boolean showEdgeWeights = true;
    public boolean showVertexValue = true;
    public boolean showVertexText = false;
    public boolean showVertexInfo = false;
    
    // Welche Informationen sollen bei Knoten und Kanten angezeigt werden
    public String[] kanteKurztext;  
    public String[] kanteLangtext;
    public String[] knotenKurztext = {"Wert"};
    public String[] knotenLangtext = {"Infotext","Wert","Markiert","Besucht"};

    // Speicheroption
    public boolean saveAsMatrix = false;

    // Farben der Anzeige
    public String[] stdFarbenKnoten = {"D0D0D0", "ACFFAB", "ACFFED", "ACC5ED", "ACC582", "E5C582", "E5C5C2", "D19FFD", "FF9F8E","FF6158", "FF61D4", "FFDE6E", "770000", "007700", "000077", "777700", "77a0e0", "9912E4", "783410", "12a94e"};
    public String[] stdFarbenKanten = {"606060", "5080FF", "D0D0D0"};
    public String[] farbenKnoten = stdFarbenKnoten;
    public String[] farbenKanten = stdFarbenKanten;

    // Auswahl der anzuzeigenden Knoten
    public int          fokusArt  = 0;   // 0 = Knoten, 1 = Kanten
    public GraphElement parent    = null;
    public int          auswahl   = 0;   // 0 = alle, 1 = Nachbarn, 2 = Single

    public GraphOptions(Graph g) {
        this.g = g;
        if(g.isGewichtet()) {
            kanteKurztext = new String[]{"Gewicht"};
            kanteLangtext = new String[]{"Gewicht","Markiert","Gelöscht"};
        } else {
            kanteKurztext = new String[]{};
            kanteLangtext = new String[]{"Markiert","Gelöscht"};
        }
    }

    public void ladeGraph(Table csvParser) {
        farbenKnoten = stdFarbenKnoten;
        farbenKanten = stdFarbenKanten;
        vertexSize = 30;

        showEdgeWeights = true;
        showVertexText = true;
        showVertexValue = false;
        showVertexInfo = false;
        bildDatei = "";

        fokusArt  = 0;   // 0 = Knoten, 1 = Kanten
        parent    = null;
        auswahl   = 0;

        for(int i = 0; i < csvParser.getRowCount(); i++) {
            TableRow row = csvParser.getRow(i);

            if(row.getString(0).equals("showWeights")) {
                showEdgeWeights = (row.getInt(1) == 1);
            }
            if(row.getString(0).equals("showInfoText")) {
                showVertexInfo = (row.getInt(1) == 1);
            }

            if(row.getString(0).equals("vertexSize")) {
                vertexSize = row.getInt(1);
            }
            if(row.getString(0).equals("vertexStyle")) {
                showVertexText = (row.getInt(1) == 1);
                showVertexValue = (row.getInt(1) == 2);
            }
            if(row.getString(0).equals("image")) {
                if(row.getString(1).charAt(0) == '0'){
                    bildDatei = "";
                    bildAnzeigen = false;
                } else {
                    bildDatei = row.getString(1);
                    bildAnzeigen = true;
                }
            }
            if(row.getString(0).equals("vertexColor")) {
                farbenKnoten = Arrays.copyOfRange(csvParser.getStringRow(i), 1, row.getColumnCount());  
            }
            if(row.getString(0).equals("edgeColor")) {
                farbenKanten = Arrays.copyOfRange(csvParser.getStringRow(i), 1, row.getColumnCount());  
            }
            if(row.getString(0).equals("matrix") || row.getString(0).equals("list")) {
                if(row.getString(0).equals("matrix"))
                    saveAsMatrix = true;
                else 
                    saveAsMatrix = false;
                break;
            }

        }
    }

    public String getText() {
        String gesamtText = "# Anzeigeoptionen:";
        gesamtText += "# Gewichte anzeigen 1, Gewichte nicht anzeigen 0\n";
        if(showEdgeWeights)
            gesamtText += "showWeights,1\n";
        else
            gesamtText += "showWeights,0\n";
        if(vertexSize != 30) {
            gesamtText += "# Größe der Knoten\n";
            gesamtText += "vertexSize,"+vertexSize+"\n";
        }
        gesamtText += "# Knoteninfo anzeigen 1,Knoteninfo nicht anzeigen 0\n";
        if(showVertexInfo)
            gesamtText += "showInfoText,1\n";
        else
            gesamtText += "showInfoText,0\n";

        gesamtText += "# Knoten leer 0, Knotenname anzeigen 1, Wert des Knoten anzeigen 2\n";
        if(!showVertexText && !showVertexValue) gesamtText += "vertexStyle,0\n";
        else {
            if(showVertexText) gesamtText += "vertexStyle,1\n";
            else
                gesamtText += "vertexStyle,2\n";
        }
        if(farbenKanten!=stdFarbenKanten) {
            gesamtText += "# Kantenfarben: RGB-Hexcode (z.B. FF0000) oder invisible\n";
            gesamtText += "# Reihenfolge: normale Kanten, markierte Kanten, gelöschte Kanten\n";
            gesamtText += "edgeColor";
            for(String f : farbenKanten) gesamtText+=","+f;
            gesamtText += "\n";
        }
        if(farbenKnoten!=stdFarbenKnoten) {
            gesamtText += "# Knotenfarben: RGB-Hexcode (z.B. FF0000) oder invisible\n";
            gesamtText += "# Reihenfolge: nichts, markiert, besucht, besucht und markiert\n";
            gesamtText += "# mind. 12 Farben müssen angegeben werden.";
            gesamtText += "vertexColor";
            for(String f : farbenKnoten) gesamtText+=","+f;
            gesamtText += "\n";
        }
        gesamtText += "# Bild im Hintergrund (bitte im \"images\"-Ordner ablegen) --> Dateiname angeben. Fall kein Bild bitte 0 schreiben!\n";
        if(bildDatei.equals(""))
            gesamtText += "image,0\n";
        else
            gesamtText += "image,"+bildDatei + "\n";
        return gesamtText;
    }

    public GraphOptions copy() {
        GraphOptions kopie = new GraphOptions(g);
        kopie.bildDatei= bildDatei;
        kopie.bildAnzeigen = bildAnzeigen;
        kopie.vertexSize = vertexSize;
        kopie.showEdgeWeights = showEdgeWeights;
        kopie.showVertexValue = showVertexValue;
        kopie.showVertexText = showVertexText;
        kopie.showVertexInfo = showVertexInfo;
        kopie.saveAsMatrix = saveAsMatrix;
        kopie.stdFarbenKnoten = stdFarbenKnoten.clone();
        kopie.stdFarbenKanten = stdFarbenKanten.clone();
        kopie.farbenKnoten = farbenKnoten.clone();
        kopie.farbenKanten = farbenKanten.clone();  
        kopie.fokusArt  = fokusArt; 
        kopie.auswahl   = auswahl;
        kopie.parent    = parent;
        kopie.kanteKurztext = kanteKurztext.clone();
        kopie.knotenKurztext = knotenKurztext.clone();
        kopie.kanteLangtext = kanteLangtext.clone();
        kopie.knotenLangtext = knotenLangtext.clone();
        return kopie;
    }

}
